# daverona/docker/modsecurity <!-- alpine docker modsecurity docker alpine -->

[![pipeline status](https://gitlab.com/daverona/docker/modsecurity/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/modsecurity/-/commits/master)

This is a repository for Alpine Docker images of [ModSecurity](https://modsecurity.org/) library.

* GitLab source repository: [https://gitlab.com/daverona/docker/modsecurity](https://gitlab.com/daverona/docker/modsecurity)
* Docker Hub repository: [https://hub.docker.com/r/daverona/modsecurity](https://hub.docker.com/r/daverona/modsecurity)

Note that all Docker images in this repository are built on `alpine` base images
with stock Apache or Nginx APK packages.
Hence our images may not be not compatible with the ones based on `httpd:alpine` or `nginx:alpine` 
which build servers from source code. 

Available versions are:

* [v2.9.3-alpine3.10](https://gitlab.com/daverona/docker/modsecurity/-/blob/v2.9.3-alpine3.10/Dockerfile)

For Apache or Nginx versions, please refer stock APK packages in each `alpine` image.

## Quick Start

To copy ModSecurity from this image to yours *not* as base image, add the following to your Dockerfile:

```dockerfile
# Replace v2.9.3-alpine3.10 with the version you want in the following
COPY --from=daverona/modsecurity:v2.9.3-alpine3.10 /usr/lib/apache2/mod_security2.so /usr/lib/apache2/mod_security2.so
COPY --from=daverona/modsecurity:v2.9.3-alpine3.10 /etc/apache2/conf.d/modsecurity.conf /etc/apache2/conf.d/modsecurity.conf
COPY --from=daverona/modsecurity:v2.9.3-alpine3.10 /etc/modsecurity.d /etc/modsecurity.d
COPY --from=daverona/modsecurity:v2.9.3-alpine3.10 /usr/local/ssdeep/*/lib /usr/local/lib
COPY --from=daverona/modsecurity:v2.9.3-alpine3.10 /usr/local/ssdeep/*/bin /usr/local/bin
RUN true \
  # Remove static libfuzzy library files (i.e. libfuzzy.a and libfuzzy.la)
  && rm -rf /usr/local/lib/libfuzzy.a /usr/local/lib/libfuzzy.la \
  # Rebuild library cache for ssdeep
  # @see https://stackoverflow.com/a/39459749
  && ldconfig /usr/local/lib /usr/lib /lib \
  # Install modsecurity dependencies
  && apk add --no-cache \
    apache2 \
    libcurl \
    libxml2 \
    lua5.3 \
    libpcrecpp \
    yajl
```

## References

* ModSecurity GitHub repository: [https://github.com/SpiderLabs/ModSecurity/](https://github.com/SpiderLabs/ModSecurity/)
* ModSecurity v2 GitHub repository: [https://github.com/SpiderLabs/ModSecurity/tree/v2.9.3](https://github.com/SpiderLabs/ModSecurity/tree/v2.9.3)
* ModSecurity-apache GitHub repository: [https://github.com/SpiderLabs/ModSecurity-apache](https://github.com/SpiderLabs/ModSecurity-apache)
* ModSecurity-nginx GitHub repository: [https://github.com/SpiderLabs/ModSecurity-nginx](https://github.com/SpiderLabs/ModSecurity-nginx)
* Official Dockerfile repository: [https://github.com/CRS-support/modsecurity-docker](https://github.com/CRS-support/modsecurity-docker)
* A comment on versions: [https://github.com/SpiderLabs/ModSecurity/issues/2176#issuecomment-537432206](https://github.com/SpiderLabs/ModSecurity/issues/2176#issuecomment-537432206)
<!-- * Compiling and Installing ModSecurity for Nginx Open Source: [https://www.nginx.com/blog/compiling-and-installing-modsecurity-for-open-source-nginx/](https://www.nginx.com/blog/compiling-and-installing-modsecurity-for-open-source-nginx/) -->
<!-- * An Alpine Dockerfile (not working): [https://github.com/SpiderLabs/ModSecurity/issues/2176](https://github.com/SpiderLabs/ModSecurity/issues/2176) -->
https://github.com/andrewnk/docker-alpine-nginx-modsec/blob/master/Dockerfile
