FROM alpine:3.10

ARG SSDEEP_VERSION=2.14.1

# Install ssdeep
# @see https://hub.docker.com/r/owasp/modsecurity/dockerfile
RUN apk add --no-cache --virtual=build-deps \
    build-base \
  && wget --quiet --output-document=- https://github.com/ssdeep-project/ssdeep/releases/download/release-$SSDEEP_VERSION/ssdeep-$SSDEEP_VERSION.tar.gz | tar -zxvf - -C /tmp \
  && cd /tmp/ssdeep-$SSDEEP_VERSION \
  && ./configure \
  && make -j $(nproc) && make install \
  && apk del --no-cache build-deps \
  && rm -rf /tmp/*

# Install modsecurity dependencies
RUN apk add --no-cache \
    geoip \
    libcurl \
    libpcrecpp \
    libxml2 \
    lmdb \
    lua5.3-libs \
    yajl \
    zlib

ARG MODSECURITY_VERSION=v3.0.4

# Install modsecurity
# @see https://hub.docker.com/r/owasp/modsecurity/dockerfile
RUN apk add --no-cache --virtual=build-deps \
    autoconf \
    automake \
    build-base \
    curl-dev \
    geoip-dev \
    git \
    libtool \
    libxml2-dev \
    linux-headers \
    lmdb-dev \
    lua5.3-dev \
    pcre-dev \
    yajl-dev \
    zlib-dev \
  && git clone --depth 1 --single-branch --branch $MODSECURITY_VERSION https://github.com/SpiderLabs/ModSecurity.git /tmp/modsecurity-$MODSECURITY_VERSION \
  && cd /tmp/modsecurity-$MODSECURITY_VERSION \
  && git submodule init && git submodule update \
  # Is this a bug of modsecurity on lua or do I miss something?
  && sed -i "s|lua-5.3 ||" build/lua.m4 \
  && ./build.sh \
  && ./configure \
    --disable-doxygen-doc \
    --with-lmdb \
    --with-ssdeep \
  && make -j $(nproc) \
  && cd test && ./regression_tests && ./unit_tests && cd .. \
  && make install \
  && apk del --no-cache build-deps \
  && rm -rf /tmp/*
